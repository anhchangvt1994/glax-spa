const _ = window._;
const $ = window.$;
const Vue = window.Vue;
const Vuex = window.Vuex;
const mapState = window?.Vuex?.mapState ?? {};
const mapGetters = window?.Vuex?.mapGetters ?? {};
const VueRouter = window.VueRouter;
const required = window.validators?.required ?? {};
const minLength = window.validators?.minLength ?? {};
const VueI18n = window?.VueI18n ?? {};

export {
  _,

  $,

  Vue,

  Vuex,
  mapState,
  mapGetters,

  VueRouter,

  required,
  minLength,

  VueI18n,
};

// NOTE - lib css thường đi cùng lib js, nếu đi riêng thì dùng common chứ không dùng priate
// NOTE - Tồng hợp những lib onpage được sử dụng kèm khi khai báo router cho page
/**
 * {
 *    css: {
 *      [key:string]: {
 *        name: string,
 *        is_loaded: boolean,
 *        crossorigin?: {
 *          show: boolean,
 *          value?: string,
 *        },
 *
 *        href: string,
 *        env?: Array<string>,
 *      }
 *    },
 *
 *    js: {
 *      [key:string]: {
 *        name: string,
 *        is_loaded: boolean,
 *        crossorigin?: {
 *          show: boolean,
 *          value?: string,
 *        },
 *        integrity?: string,
 *
 *        src: string,
 *        env?: Array<string>,
 *      }
 *    },
 * }
 */
export const ROUTER_RESOURCE = {
  css: {
    'swiper-slider': {
      name: 'swiper-slider',
      is_loaded: false,
      href: 'https://cdnjs.cloudflare.com/ajax/libs/Swiper/6.5.8/swiper-bundle.min.css',
    },
  },
  js: {
    'swiper-slider': {
      name: 'swiper-slider',
      is_loaded: false,
      src: 'https://cdnjs.cloudflare.com/ajax/libs/Swiper/6.5.8/swiper-bundle.min.js',
    }, // swiper-slider
  },
};
