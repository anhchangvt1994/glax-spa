import EventBus from './PubSub';

export const GlobalService = (() => {
  return {
    init() {
      window.EventBus = EventBus;
    },
  };
})(); // GlobalService()
