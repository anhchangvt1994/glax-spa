const _addEvent = (ev) => {
  if(!ev) {
    console.error('The event should be undefined!');
    return;
  }

  return function(el, fn) {
    if (el.addEventListener) {
      el.addEventListener(ev, fn, false);
    } else if (el.attachEvent) {
        el.attachEvent('on' + ev, fn);
    } else {
        el['on' + ev] = fn;
    }
  }
}; // _addEvent()

const onloadEvent = _addEvent('load');

export {
  onloadEvent,
};
