import { _ } from '~jsPath/lib/lib-manager';

const MODULE_NAME = 'pageInfoModule';

const MUTATIONS_KEY = {
  setImageUrl: 'setImageUrl',
  setResManagementData: 'setResManagementData',
};

const COMMIT_KEY = {
  setImageUrl: MODULE_NAME + '/' + MUTATIONS_KEY.setImageUrl,
  setResManagementData: MODULE_NAME + '/' + MUTATIONS_KEY.setResManagementData,
};

const pageInfoModule = {
  namespaced: true,

  state: {
    imageUrl: null,
    resManagementData: [],
  },

  getters: {
    managementList(state) {
      if(_.isEmpty(state.resManagementData)) {
        return [];
      }

      return state.resManagementData.management_list;
    },
  },

  mutations: {
    [MUTATIONS_KEY.setImageUrl](state, imageUrl) {
      state.imageUrl = imageUrl;
    },

    [MUTATIONS_KEY.setResManagementData](state, resManagementData) {
      state.resManagementData = resManagementData;
    },
  },
};

export {
  MODULE_NAME,
  MUTATIONS_KEY,
  COMMIT_KEY,
  pageInfoModule,
};
