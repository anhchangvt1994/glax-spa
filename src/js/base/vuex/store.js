import {
  Vue,
  Vuex
} from '~jsLibPath/lib-manager';
import {
  ENV,
  ARR_ENV,
} from '~jsDefinePath/general';
import {
  pageInfoModule,
} from './modules/page-info';

Vue.use(Vuex);

Vue.config.devtools = (ENV !== ARR_ENV.production);

const store = new Vuex.Store({
  strict: true,
  modules: {
    pageInfoModule,
  },
});

export {
  store,
};
