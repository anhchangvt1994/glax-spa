import {
  VueI18n,
  _
} from '~jsLibPath/lib-manager';
import EventBus from '~jsPath/helper/service/PubSub';
import { KEYPATH_CONFIG } from '~jsDefinePath/language';

const messages = KEYPATH_CONFIG;

const i18n = new VueI18n({
  locale: 'en',
  messages
});

EventBus.on('UPDATE_LANGUAGE_KEYPATH', function(objUpdateInfo) {
  objUpdateInfo.locale = objUpdateInfo.locale || 'vi';

  if(
    _.isEmpty(objUpdateInfo) ||
    !objUpdateInfo.name ||
    _.isEmpty(objUpdateInfo.keypath) ||
    _.isEmpty(messages[objUpdateInfo.locale]) ||
    !_.isEmpty(messages[objUpdateInfo.locale][objUpdateInfo.name])
  ) {
    return;
  }

  messages[objUpdateInfo.locale][objUpdateInfo.name] = objUpdateInfo.keypath;

  EventBus.emit('IS_GET_LANGUAGE_FINISH--' + objUpdateInfo.name);
});

export {
  i18n
};
