import { VueRouter } from '~jsLibPath/lib-manager';
import {
  ROUTER_NAME,
  ROUTER_INFO,
} from '~jsBasePath/vue-router/router-enum';
import RouterUtil from './router-util';

// NOTE - Khai báo router info, mỗi khi tạo 1 router mới thì cần đăng ký bên file router-enum và dùng thông tin đăng ký đó để khai báo cho phần router bên này
const router = new VueRouter({
  mode: 'history',
  routes: [
    {
      path: ROUTER_INFO[ROUTER_NAME['home-page']].path,
      name: ROUTER_NAME['home-page'],
    },
    {
      path: ROUTER_INFO[ROUTER_NAME['list-task-page']].path,
      name: ROUTER_NAME['list-task-page'],
    },

    {
      path: ROUTER_INFO[ROUTER_NAME['list-task-detail']].path,
      name: ROUTER_NAME['list-task-detail'],
    },
    {
      path: '*',
      name: '404-not-found',
      component: () => import('~jsPath/partial/not-found-page/TheNotFoundPage.vue'),
    }
  ],
});

RouterUtil.init(router);

export {
  router
};
