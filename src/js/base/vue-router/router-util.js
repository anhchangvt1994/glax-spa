import { CACHE_VERSION, ENV } from '~jsDefinePath/general';
import {
  _,
  $,
  ROUTER_RESOURCE
} from '~jsLibPath/lib-manager';
import {
  ROUTER_INFO,
  ROUTER_NAME,
  ROUTER_PATH
} from '~jsBasePath/vue-router/router-enum';

export default (() => {
  // NOTE - Xừ lý cho beforeEach router, cần họp lại nếu muốn thay đổi logic
  let routerTarget = null;
  const arrCssStyle = {};
  const arrLanguagePicked = {};
  let toMatchLastIndex = 0;

  const _doAfterElScriptFinish = (
    elScript,
    to,
    from,
    next,
  ) => {
    document.head.appendChild(elScript);

    elScript.onload = function() {
      to.matched[toMatchLastIndex].components.default = ROUTER_INFO[to.name].component();

      EventBus.emit('UPDATE_ROUTER_VIEW', Date.now());

      if(!ROUTER_INFO[to.name].no_style) {
        // NOTE - Thêm style tag của router view đó vào danh sách style (router view nào cũng phải có style tag, không nhiều thì ít)
        const elStyle = document.getElementsByTagName('style');
        arrCssStyle[to.name] = elStyle[elStyle.length - 1];
      }

      // NOTE - Nếu style tag của router trước có tồn tại thì xoá đi chú ý là nên xoá sau khi next() router
      if(
        to.name !== from.name &&
        arrCssStyle[from.name]
      ) {
        arrCssStyle[from.name].remove();
      }
    };

    elScript.onerror = function() {
      next('/404-not-found');
      elScript.remove();

      EventBus.emit('UPDATE_ROUTER_VIEW', Date.now());
    };
  }; // _doAfterElScriptFinish()

  const _listenBeforeEach = () => {
    routerTarget.beforeEach((to,from,next) => {
      if(!to.matched.length) {
        console.error('this router is not defined before!');
        return;
      } else if(to.name === '404-not-found') {
        setInterval(function() {
          EventBus.emit('IS_SHOW_ROUTER_LOADING', false);
          next();
        });

        return;
      }

      toMatchLastIndex = to.matched.length - 1;

      if(
        ROUTER_INFO[to.name].permisson &&
        typeof ROUTER_INFO[to.name].permisson.valid === 'function'
      ) {
        const isPermisonValid = ROUTER_INFO[to.name].permisson.valid.call({
          to: to,
          from: from,
        });

        if(!isPermisonValid) {
          const redirect = (
            ROUTER_INFO[to.name].permisson.redirect ?
            ROUTER_INFO[to.name].permisson.redirect :
            ROUTER_PATH[ROUTER_NAME['home-page']]
          );

          next(redirect);
          return;
        }
      }

      if(
        !to.matched[toMatchLastIndex].components.default &&
        ROUTER_INFO[to.name].path
      ) {
        next();
        EventBus.emit('IS_SHOW_ROUTER_LOADING', true);

        const elScript = document.createElement('script');
        elScript.setAttribute('src', ROUTER_INFO[to.name].component_path);

        let isGetLanguageFinish = true;

        // NOTE - Nếu có khai báo language on page thì thực hiện load nội dung language
        if(
          ROUTER_INFO[to.name].language &&
          !arrLanguagePicked[to.name]
        ) {
          isGetLanguageFinish = false;

          EventBus.once('IS_GET_LANGUAGE_FINISH--' + ROUTER_INFO[to.name].language.name, function() {
            isGetLanguageFinish = true;
          });

          $.getJSON(ROUTER_INFO[to.name].language.path)
          .done(function(objResponse) {
            if(_.isEmpty(objResponse)) {
              // NOTE - do something after empty (same error)
              arrLanguagePicked[to.name] = false;
            }

            arrLanguagePicked[to.name] = true;
            EventBus.emit('UPDATE_LANGUAGE_KEYPATH', {
              name: ROUTER_INFO[to.name].language.name,
              keypath: objResponse,
              locale: 'en',
            })
          })
          .fail(function() {
            // NOTE - do something after error
            arrLanguagePicked[to.name] = false;
          })
        }

        // NOTE - Nếu có khai báo js on page thì thực hiện load js tuần tự
        if(
          ROUTER_INFO[to.name].js &&
          ROUTER_INFO[to.name].js.length
        ) {
          let intTotalOnPageScript = 0;
          let intTotalOnPageScriptFinish = 0;

          ROUTER_INFO[to.name].js.forEach(function(routerJsName) {
            if(!routerJsName) {
              return;
            }

            intTotalOnPageScript++;

            const jsPicked = ROUTER_RESOURCE.js[routerJsName];

            if(
              jsPicked &&
              jsPicked.src &&
              !jsPicked.is_loaded &&
              (
                !jsPicked.env ||
                jsPicked.includes(ENV)
              )
            ) {
              // NOTE - Định nghĩa script tag cho js lib hiện tại
              const elOnPageScript = document.createElement('script');
              elOnPageScript.setAttribute('src', jsPicked.src);

              if(jsPicked.integrity) {
                elOnPageScript.setAttribute('integrity', jsPicked.integrity);
              }

              if(
                jsPicked.crossorigin &&
                jsPicked.crossorigin.is_show
              ) {
                elOnPageScript.setAttribute('crossorigin', jsPicked.crossorigin.value);
              }

              // NOTE -  Định nghĩa link style cho css lib hiện tại
              let elOnPageStyle = null;

              if(
                ROUTER_RESOURCE.css[routerJsName] &&
                ROUTER_RESOURCE.css[routerJsName].href
              ) {
                elOnPageStyle = document.createElement('link');
                elOnPageStyle.rel = "stylesheet";
                elOnPageStyle.href = ROUTER_RESOURCE.css[routerJsName].href + '?v=' + CACHE_VERSION;

                document.head.appendChild(elOnPageStyle);
              }

              document.head.appendChild(elOnPageScript);

              elOnPageScript.onload = function() {
                intTotalOnPageScriptFinish++;

                ROUTER_RESOURCE.js[routerJsName].is_loaded = true;

                if(intTotalOnPageScriptFinish === intTotalOnPageScript) {
                  const doAfterScriptFinishTimeout = setInterval(function() {
                    if(isGetLanguageFinish) {
                      _doAfterElScriptFinish(elScript, to, from, next);

                      clearInterval(doAfterScriptFinishTimeout);
                    }
                  });
                }
              };
            } else {
              intTotalOnPageScriptFinish++;
            }
          });

          if(intTotalOnPageScript === intTotalOnPageScriptFinish) {
            const doAfterScriptFinishTimeout = setInterval(function() {
              if(isGetLanguageFinish) {
                _doAfterElScriptFinish(elScript, to, from, next);

                clearInterval(doAfterScriptFinishTimeout);
              }
            });
          }
        } else {
          const doAfterScriptFinishTimeout = setInterval(function() {
            if(isGetLanguageFinish) {
              _doAfterElScriptFinish(elScript, to, from, next);

              clearInterval(doAfterScriptFinishTimeout);
            }
          });
        }
      } else {
        if(arrCssStyle[to.name]) {
          document.head.append(arrCssStyle[to.name]);
        } else if(!ROUTER_INFO[to.name].no_style) {
          // NOTE - Thêm style tag của router view đó vào danh sách style (router view nào cũng phải có style tag, không nhiều thì ít)
          const elStyle = document.getElementsByTagName('style');
          arrCssStyle[to.name] = elStyle[elStyle.length - 1];
        }

        next();

        // NOTE - Nếu style tag của router trước có tồn tại thì xoá đi chú ý là nên xoá sau khi next() router
        if(
          to.name !== from.name &&
          arrCssStyle[from.name]
        ) {
          arrCssStyle[from.name].remove();
        }
      }
    });
  }; // _listenBeforeEach()

  return {
    init(router) {
      if(
        routerTarget ||
        !router
      ) {
        return;
      }

      routerTarget = router;

      _listenBeforeEach();
    }
  };
})(); // RouterUtil()
