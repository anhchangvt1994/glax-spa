import {
  JS_URL,
  CACHE_VERSION
} from '~jsDefinePath/general';
import {
  LANGUAGE_NAME,
  LANGUAGE_INFO,
} from '~jsDefinePath/language';
import { ROUTER_RESOURCE } from '~jsLibPath/lib-manager';

const _generateComponentPath = (strFileName) => {
  return JS_URL + '/' + strFileName + '?v=' + CACHE_VERSION;
}; // _generateComponentPath()

// NOTE - Xây dựng tham chiếu đến window.router_view_component_list
const ROUTER_VIEW_COMPONENT_LIST = {
  TheHomePage: null,
  TheListTaskPage: null,
  TheListTaskDetailPage: null,
};

// NOTE - Định nghĩa object router_view_components
window.router_view = ROUTER_VIEW_COMPONENT_LIST;

const BASE_PATH = '/';

// NOTE - Định nghĩa danh sách tên của router (chú ý: tên của router nên là tên của component router_view đó hoặc là kiểu snack của component name.)
// NOTE - VD: component có name: TheHomePage / HomePage / home-page thì router name sẽ đặt là "home-page", nên đặt cùng ý nghĩa, vì sẽ có những trường hợp cần so sánh router name và component name để thực hiện một action nào đó
const ROUTER_NAME = {
  'home-page': 'home-page',
  'list-task-page': 'list-task-page',
  'list-task-detail': 'list-task-detail',
};

// NOTE - Định nghĩa danh sách path của router, hiện tại home-page sẽ là path default "/", tức là BASE_PATH. đặt tên cho path không cần phải giống với router name (VD: router_name: list-task-page)
const ROUTER_PATH = {
  // NOTE - home page group
  [ROUTER_NAME['home-page']]: BASE_PATH,

  // NOTE - list task group
  [ROUTER_NAME['list-task-page']]: BASE_PATH + 'list-task-page',
  [ROUTER_NAME['list-task-detail']]: BASE_PATH + 'list-task-page/:id',
};

// NOTE - Định nghĩa danh sách component file name cho từng router, vì những file này đã được tách ra khỏi app.js file (tối ưu dung lượng các resource onpage loading - không cho phép 1 resource có dung lượng từ 200kb được onpage loading) nên cần được định nghĩa vào đây thay vì import trực tiếp vào, khi router được kích hoạt sẽ có phần tiền xử lý để kiểm tra router đã được đăng ký hay chưa để append tài nguyên của router đó vào
const ROUTER_COMPONENT_FILE_NAME = {
  // NOTE - home page group
  [ROUTER_NAME['home-page']]: 'the-home-page.js',

  // NOTE - list task group
  [ROUTER_NAME['list-task-page']]: 'the-list-task-page.js',
  [ROUTER_NAME['list-task-detail']]: 'the-list-task-detail-page.js',
};

// NOTE - Định nghĩa danh sách toàn bộ thông tin của mỗi router, bao gồm
/**
 * {
 *  name: string,
 *  path: string,
 *  component_path: string,
 *  component: Function,
 *
 *  no_style?: boolean,
 *
 *  permisson?: {
 *    valid: function(loginInfo) { this { to: ..., from: ... } }, // NOTE - Phải sử dụng traditional function vì yêu cầu context this từ router-util
 *    redirect: string,
 *  },
 *
 *  js?: Array<string>,
 *
 *  language?: Object, // NOTE - Path of language on page
 * }
*/
const ROUTER_INFO = {
  // NOTE - home page group
  [ROUTER_NAME["home-page"]]: {
    name: ROUTER_NAME['home-page'],
    path: ROUTER_PATH[ROUTER_NAME['home-page']],
    component_path: _generateComponentPath(ROUTER_COMPONENT_FILE_NAME[ROUTER_NAME['home-page']]),
    component: () => (ROUTER_VIEW_COMPONENT_LIST.TheHomePage || null),

    permisson: {
      valid: function() {
        return true;
      },
      redirect: ROUTER_PATH[ROUTER_NAME['list-task-page']],
    },

    css: [
      ROUTER_RESOURCE.css['swiper-slider'].name
    ],

    js: [
      ROUTER_RESOURCE.js['swiper-slider'].name
    ],

    language: LANGUAGE_INFO[LANGUAGE_NAME['the-home-language']],
  },

  // NOTE - list task group
  [ROUTER_NAME["list-task-page"]]: {
    name: ROUTER_NAME['list-task-page'],
    path: ROUTER_PATH[ROUTER_NAME['list-task-page']],
    component_path: _generateComponentPath(ROUTER_COMPONENT_FILE_NAME[ROUTER_NAME['list-task-page']]),
    component: () => (ROUTER_VIEW_COMPONENT_LIST.TheListTaskPage || null),

    // js: [
    //   ROUTER_RESOURCE.js['vue2-google-maps'].name
    // ]
  },

  [ROUTER_NAME["list-task-detail"]]: {
    name: ROUTER_NAME['list-task-detail'],
    path: ROUTER_PATH[ROUTER_NAME['list-task-detail']],
    component_path: _generateComponentPath(ROUTER_COMPONENT_FILE_NAME[ROUTER_NAME['list-task-detail']]),
    component: () => (ROUTER_VIEW_COMPONENT_LIST.TheListTaskDetailPage || null),
    no_style: true,
  },
};

export {
  ROUTER_NAME,
  ROUTER_PATH,
  ROUTER_INFO,
  ROUTER_VIEW_COMPONENT_LIST,
};
