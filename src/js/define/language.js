import {
  PAGE_INFO,
  LANGUAGE_URL
} from './general';

const LANGUAGE_NAME = {
  'the-home-language': 'the-home-language',
};

const LANGUAGE_PATH = {
  [LANGUAGE_NAME["the-home-language"]]: LANGUAGE_URL + '/the-home-language.json',
};

const LANGUAGE_INFO = {
  [LANGUAGE_NAME['the-home-language']]: {
    name: LANGUAGE_NAME['the-home-language'],
    path: LANGUAGE_PATH[LANGUAGE_NAME['the-home-language']],
  }
};

const KEYPATH_CONFIG = PAGE_INFO?.resManagementData?.data?.language ?? {
  vi: {},
  en: {},
};

export {
  LANGUAGE_NAME,
  LANGUAGE_PATH,
  LANGUAGE_INFO,
  KEYPATH_CONFIG,
}
