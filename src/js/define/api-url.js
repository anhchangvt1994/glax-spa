import { getApiUrl } from './general';

export const BASE_API_URL = getApiUrl('v1');

export const AJAX_URL = {
  user: {
    login: BASE_API_URL
  }
};
