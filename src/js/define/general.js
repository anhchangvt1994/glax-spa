export const ARR_ENV = {
  development: 'development',
  staging: 'staging',
  production: 'prodution',
};

export const PAGE_INFO = window?.PAGE_INFO ?? {};

export const IMAGE_URL = PAGE_INFO?.image_url ?? '';
export const JS_URL = PAGE_INFO?.js_url ?? '';
export const LANGUAGE_URL = PAGE_INFO?.language_url ?? '';
export const ENV = PAGE_INFO?.env ?? ARR_ENV.development;
export const CACHE_JS_VERSION = Date.now();
export const CACHE_VERSION = PAGE_INFO?.cache_version ?? CACHE_JS_VERSION;
export const getApiUrl = PAGE_INFO?.get_api_url ?? null;
