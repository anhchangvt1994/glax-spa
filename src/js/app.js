import {
  IMAGE_URL,
  PAGE_INFO,
  ARR_ENV,
  ENV
} from '~jsDefinePath/general';
import {
  Vue,
  VueRouter
} from '~jsLibPath/lib-manager';
import { store } from '~jsBasePath/vuex/store';
import { COMMIT_KEY } from '~jsBasePath/vuex/modules/page-info';
import { router } from '~jsBasePath/vue-router/router';
import { i18n } from '~jsBasePath/vue-i18n/translate';
import { GlobalService } from '~jsHelperPath/service/service-manager';
import VueTemplate from './app.vue';

// import * as VueGoogleMaps from 'vue2-google-maps';

const App = (() => {
  const __initVueApp = () => {
    let elApp = document.getElementById('app');

    if(!elApp) {
      elApp = document.createElement('div');
      elApp.id = 'app';

      document.body.appendChild(elApp);
    }

    return {
      success: true,
      target: elApp,
    };
  }; // __initVueApp()

  const __initSetupPageInfoModule = () => {
    store.commit(COMMIT_KEY.setImageUrl, IMAGE_URL);

    store.commit(COMMIT_KEY.setResManagementData, PAGE_INFO?.resManagementData?.data);
  }; // __initSetupPageInfoModule()

  const _renderApp = () => {
    const vueApp = __initVueApp();

    if(vueApp.success) {
      __initSetupPageInfoModule();

      Vue.use(VueRouter);

      Vue.config.productionTip = false;

      const app = new Vue({
        store,
        render: (h) => h(VueTemplate),
        i18n,
        router,
      }).$mount(vueApp.target);

      window.__VUE_DEVTOOLS_GLOBAL_HOOK__.Vue = (ENV === ARR_ENV.development ? app.constructor : null);
    }
  }; // _renderApp()

  return {
    init() {
      GlobalService.init();

      _renderApp();
    },
  }
})();

App.init();
