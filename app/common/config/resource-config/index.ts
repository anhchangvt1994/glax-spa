import APP from '@common/enum/source-enum';
import {
  DEVELOPMENT,
  PRODUCTION,
  STAGING,
} from '@common/define/enviroment-define';
import './resource-config-interface';

export let BASE_URL:string;
export let BASE_STATIC_URL:string;
export let BASE_RESOURCE_URL:string;
export let BASE_API_URL: string;

export const RESOURCE: ResourceConstruct = {
  'project': 'gulp',
  'port': process.env.PORT || 3000,
  'ip_address': null,

  'staging_base_url': 'staging.go2joy.vn',
  'staging_static_url': 'staging.go2joy.vn',

  'base_url': 'go2joy.vn',
  'static_url': 'go2joy.vn',

  'staging_base_api_url': 'staging-api.go2joy.vn/api',
  'base_api_url': 'production-api.go2joy.vn/api',

  'api_version': {
    'v1': 'v1',
  },

  'local': 'localhost',

  'path': {
    'src': APP.src.path,
    'dummy_data': APP.src.dummy_data,
    'njk': APP.src.njk,
    'global': APP.src.njk + '/global',
    'layout': APP.src.njk + '/_layout.njk',
  },

  'resource': {
    "libs" : {
      "arrCssFile": [],
      "arrJsFile": []
    },

    "common" : {
      "arrCssFile": [
        {
          'preconnect': 'https://fonts.gstatic.com',
          'href': 'https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700&family=Oswald:wght@400;500;600;700&family=Quicksand:wght@400;500;600;700&display=swap',
        },
        {
          'preconnect': 'https://pro.fontawesome.com',
          'href': 'https://pro.fontawesome.com/releases/v5.2.0/css/all.css',
        },
      ],
      "arrJsFile": [
        {
          'preconnect': 'https://cdnjs.cloudflare.com',
          'dns-prefetch': 'https://cdnjs.cloudflare.com',
          'crossorigin': {
            show: true,
            value: 'anonymous',
          },
          'src': 'https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.21/lodash.min.js',
          'integrity': 'sha512-WFN04846sdKMIP5LKNphMaWzU7YpMyCU245etK3g/2ARYbPK9Ub18eG+ljU96qKRCWh+quCY7yefSmlkQw1ANQ==',
        },
        {
          'src': 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js',
          'integrity': 'sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==',
          'crossorigin': {
            show: true,
            value: 'anonymous',
          },
        },
        {
          'src': 'https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.12/vue.min.js',
          'integrity': 'sha512-BKbSR+cfyxLdMAsE0naLReFSLg8/pjbgfxHh/k/kUC82Hy7r6HtR5hLhobaln2gcTvzkyyehrdREdjpsQwy2Jw==',
          'crossorigin': {
            show: true,
            value: 'anonymous',
          },
        },
        {
          'src': 'https://cdnjs.cloudflare.com/ajax/libs/vuex/3.6.2/vuex.min.js',
          'integrity': 'sha512-Tkxwo8dZEZTmje5QT9uodCqe2XGbZdBXU8uC4nskBt0kwR99Anzkz8JCSMByfoqjLTHcTuIB8fsmED3b9Ljp3g==',
          'crossorigin': {
            show: true,
            value: 'anonymous',
          },
        },
        {
          'src': 'https://cdnjs.cloudflare.com/ajax/libs/vue-router/3.5.1/vue-router.min.js',
          'integrity': 'sha512-c5QVsHf336TmWncPySsNK2ncFiVsPEWOiJGDH/Le/5U6q1hU6F5B7ziAgRwCokxjmu4HZBkfWsQg/D/+X3hFww==',
          'crossorigin': {
            show: true,
            value: 'anonymous',
          },
        },
        {
          'src': 'https://cdn.jsdelivr.net/npm/vue-i18n@8.24.3/dist/vue-i18n.min.js',
        },
        {
          'preconnect': 'https://cdn.jsdelivr.net',
          'dns-prefetch': 'https://cdn.jsdelivr.net',
          'src': 'https://cdn.jsdelivr.net/npm/vuelidate@0.7.6/dist/vuelidate.min.js',
        },
        {
          'src': 'https://cdn.jsdelivr.net/npm/vuelidate@0.7.6/dist/validators.min.js',
        },
        {
          'src': 'https://cdnjs.cloudflare.com/ajax/libs/dayjs/1.10.4/dayjs.min.js',
          'integrity': 'sha512-0fcCRl828lBlrSCa8QJY51mtNqTcHxabaXVLPgw/jPA5Nutujh6CbTdDgRzl9aSPYW/uuE7c4SffFUQFBAy6lg==',
          'crossorigin': {
            show: true,
            value: 'anonymous',
          },
        },
      ]
    },

    "index" : {
      'name': 'index',
      "arrCssFile" : [
        'vendor-style'
      ],
      "arrJsFile" : [
        "app"
      ],

      'arrPreconnect': [
        () => BASE_STATIC_URL,
      ],

      'arrPreload': [
        {
          'getHref': () => BASE_STATIC_URL + '/image/go2joy-loading.gif',
          'as': 'image',
          'type': 'image/gif'
        },
        {
          'getHref': () => BASE_RESOURCE_URL + '/css/vendor-style.css',
          'as': 'style',
        },
        {
          'getHref': () => BASE_RESOURCE_URL + '/js/app.js',
          'as': 'script',
        },
      ],

      'arrPrefetch': [
        {
          'getHref': () => BASE_RESOURCE_URL + '/js/the-home-page.js',
          'as': 'script',
        },
        {
          'getHref': () => BASE_STATIC_URL + '/language/the-home-language.json',
          'as': 'fetch',
        },
        {
          'getHref': () => BASE_RESOURCE_URL + '/js/the-list-task-page.js',
          'as': 'script',
        },
        {
          'getHref': () => BASE_RESOURCE_URL + '/js/the-list-task-detail-page.js',
          'as': 'script',
        },
      ],

      'dummy_data': true,
      'dummy_data_name': 'app'
    },

    'style-guide-page': {
      'arrCssFile': [],
      'arrJsFile': {
        'common': [
          {
            'src': 'https://cdn.jsdelivr.net/npm/vue@2.6.12/dist/vue.min.js',
            'integrity': 'sha256-KSlsysqp7TXtFo/FHjb1T9b425x3hrvzjMWaJyKbpcI=',
          },
          {
            'src': 'https://cdn.jsdelivr.net/npm/vue-router@3.5.1/dist/vue-router.min.js',
            'integrity': 'sha256-eZzOaXKNUwCBbixtwh69SAgFtFqGT6028WLW01MNPKA=',
          },
        ],
        'main': [
          "style-guide-app"
        ],
      },
      'dummy_data': false,
    },
  },

  'dummy_data_name_map': {
    'app': 'index'
  },
};

//=======================================
// NOTE - generate External IP
//=======================================
const os = require('os');
const OSNetworkInterfaces = os.networkInterfaces();
const Ethernet = OSNetworkInterfaces.Ethernet || Object.values(OSNetworkInterfaces);

if(Ethernet) {
  Ethernet.some(function(ethernetItem) {
    const ethernetItemInfo = (
      (
        ethernetItem.family &&
        ethernetItem.family.toLowerCase() === 'ipv4'
      ) ? ethernetItem :
      (
        ethernetItem[1] &&
        ethernetItem[1].family.toLowerCase() === 'ipv4'
      ) ?
      ethernetItem[1] :
      (
        ethernetItem[0] &&
        ethernetItem[0].family.toLowerCase() === 'ipv4'
      ) ?
      ethernetItem[0] :
      null
    );

    if(
      ethernetItemInfo &&
      ethernetItemInfo.address !== '127.0.0.1'
    ) {
      RESOURCE.ip_address = ethernetItemInfo.address;
      return true;
    }
  });
}

if(process.env.NODE_ENV === DEVELOPMENT) {
  BASE_URL = 'http://' + RESOURCE.ip_address + ':' + RESOURCE.port;
  BASE_STATIC_URL = 'http://' + RESOURCE.ip_address + ':' + RESOURCE.port;
  BASE_RESOURCE_URL = 'http://' + RESOURCE.ip_address + ':' + RESOURCE.port + '/tmp';
  BASE_API_URL = 'https://' + RESOURCE.staging_base_api_url;
} else if (process.env.NODE_ENV === STAGING) {
  BASE_URL = 'https://' + RESOURCE.staging_base_url;
  BASE_STATIC_URL = 'https://' + RESOURCE.staging_static_url;
  BASE_RESOURCE_URL = 'https://' + RESOURCE.staging_static_url;
  BASE_API_URL = 'https://' + RESOURCE.base_api_url;
} else if (process.env.NODE_ENV === PRODUCTION) {
  BASE_URL = 'https://' + RESOURCE.base_url;
  BASE_STATIC_URL = 'https://' + RESOURCE.static_url;
  BASE_RESOURCE_URL = 'https://' + RESOURCE.static_url;
  BASE_API_URL = 'https://' + RESOURCE.base_api_url;
}
