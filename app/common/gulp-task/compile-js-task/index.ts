import { isEmpty as _isEmpty, forIn as _forIn } from 'lodash';

import modules, {
  browserSync,
  uglify,
  tinyify,
  gSourceMaps,
  fs
} from '@common/define/module-define';
import APP from '@common/enum/source-enum';
import {
  STATE_KEYS,
  ACTION_KEYS,
  MUTATION_KEYS,
  GulpTaskStore,
} from '@common/gulp-task/store';
import { ARR_FILE_EXTENSION } from '@common/define/file-define';
import { generateTmpDirItemConstruct } from '@common/enum/tmp-directory-enum';

export default class ComipleJsTask {
  constructor() {};

  getTmp() {
    const self = this;

    return {
      name: 'jsTmp',
      init:  function() {
        let _isError = false;

        let _filePathDataCssChange = [];

        let _curFilePath = null;

        // NOTE - Dùng để nạp tất cả files JS đang và sẽ được compile trong lượt run task đó (value là đường path của file nên giá trị sẽ là unique)
        let _arrJSCompileFile = [];

        // NOTE - Tính số lần hoàn thành compile để so sánh với số lượng file cần được compile
        let _totalComplete = 0;

        modules.gulp.task('jsTmp', function() {
          let _arrJsErrorFileList = [];

          let _compileTimeout = null;

          return modules.gulp.src([
              APP.src.js + '/**/*.js',
              APP.src.js + '/**/*.vue',
              APP.src.js + '/partial/**/*.css',
              APP.src.js + '/landing/**/*.css'
            ]
          )
          .pipe(gSourceMaps.init({ loadMaps: true }))
          .pipe(gSourceMaps.identityMap())
          .pipe(modules.plumber({
            'errorHandler': function(err) {
              console.log(err);
              _arrJsErrorFileList.push(err.fileName);
            },
          }))
          .pipe(modules.cached('.js'))
          .pipe(modules.eslint())
          .pipe(modules.eslint.result(function(result) {
            if(result.warningCount > 0 || result.errorCount > 0) {
              _isError = true;

              const errorMessage = result.messages[0];

              GulpTaskStore.get(STATE_KEYS.handler_error_util).handlerError({
                plugin: 'gulp-eslint',
                lineNumber: errorMessage.line,
                fileName: result.filePath,
                message: errorMessage.message
              }, ARR_FILE_EXTENSION.JS, GulpTaskStore.get(STATE_KEYS.is_first_compile_all));
            }
          }))
          .pipe(modules.tap(function(file) {
            const filePath = file.path.replace(/\\/g, '/');
            // NOTE split file.path và lấy tên file cùng tên folder để rename đúng tên cho file js phía tmp
            const filename = filePath.split('/').slice(-2)[1];
            const foldername = filePath.split('/').slice(-2)[0];

            let filePathData = null;

            // NOTE - Nếu file được change không phải là css file thì thực hiện truy vấn graph dependencies bình thường
            if(filename.indexOf(ARR_FILE_EXTENSION.CSS) === -1) {
              if(
                filename === 'app.js' ||
                (
                  filename.indexOf('-page.js') !== -1 &&
                  foldername.indexOf('-page') !== -1
                )
              ) {
                // NOTE Khi một file index thay đổi thì nó sẽ tự build lại, nên trong xử lý dependent chỉ update lại các dependents file của file index đó, chứ hok return ra mảng index cần build lại
                filePathData = GulpTaskStore.get(STATE_KEYS.js_dependents).generate({
                  'folder-name': foldername,
                  'path': filePath,
                  'file-name': filename,
                  'content': file.contents,
                  'main': true,
                });
              } else {
                filePathData = GulpTaskStore.get(STATE_KEYS.js_dependents).generate({
                  'folder-name': foldername,
                  'file-name': filename,
                  'path': filePath,
                  'content': file.contents,
                });
              }
            } else if(!GulpTaskStore.get(STATE_KEYS.is_first_compile_all)) {
              const targetJs = _generateJsTargetAfterCssChange(
                filePath,
                filename,
                foldername,
              );

              if(_filePathDataCssChange.indexOf(targetJs) === -1) {
                _filePathDataCssChange.push(targetJs);
              }

              if(_compileTimeout) {
                clearTimeout(_compileTimeout);
                _compileTimeout = null;
              }

              _compileTimeout = setTimeout(function() {
                _compileLoop(_filePathDataCssChange);

                _compileTimeout = null;
              }, 100);

              return;
            }

            if(filePathData) {
              _compileLoop(filePathData);
            }
          }))
        });

        function _compileLoop(filePathData) {
          filePathData.forEach(function(strFilePath) {
            strFilePath = strFilePath.replace(/\\/g, '/');

            // NOTE - Nạp file vào trong danh sách xử lý (danh sách này sẽ bao gồm tất cả file của task runner compile js chứ không riêng những file sau quá trình first compile)
            _arrJSCompileFile.push(strFilePath);

            let filename = strFilePath.split('/').slice(-2)[1];
            const foldername = strFilePath.split('/').slice(-2)[0];

            filename = self._generateOtherAppName(filename, foldername).replace('.js', '');

            modules.browserify({
              entries: [strFilePath],
              debug: true,
            }) // path to your entry file here
            .transform(modules.vueifyBabel7, {
              sourceMaps: true,
            })
            .transform(modules.babelify, {
              presets: [
                "@babel/preset-env",
              ],
              sourceMaps: true,
            })
            .transform("aliasify")
            .external('vue') // remove vue from the bundle, if you omit this line whole vue will be bundled with your code
            .bundle()
            .pipe(modules.source(filename + '.' + ARR_FILE_EXTENSION.JS))
            .pipe(modules.print(
              filepath => {
                return modules.ansiColors.yellow(`compile js: ${filepath}`);
              }
            ))
            .pipe(modules.rename(function(path) {
              // NOTE Nếu construct JS đối với path file name hiện tại đang rỗng thì nạp vào
              if(!GulpTaskStore.get(STATE_KEYS.tmp_construct)[ARR_FILE_EXTENSION.JS][filename]) {
                GulpTaskStore.dispatch(ACTION_KEYS.generate_tmp_construct, generateTmpDirItemConstruct({
                  extension: ARR_FILE_EXTENSION.JS,
                  file_name: filename,
                  file_path: APP.tmp.js + '/' + filename,
                }));
              }

              if(!GulpTaskStore.get(STATE_KEYS.is_first_compile_all)) {
                modules.fs.writeFile(APP.log.path + '/tmp-construct/tmp-construct-log.json', JSON.stringify(GulpTaskStore.get(STATE_KEYS.tmp_construct)), (err) => {
                  if(err) throw err;

                  console.log('write file: "tmp-construct-log.json" finish.');
                });
              }
            }))
            .pipe(modules.buffer())
            .on('error', function() {
              this.emit('end');
            })
            .pipe(gSourceMaps.write())
            .pipe(
              modules.gulp.dest(APP.tmp.js)
            )
            .on('end', function() {
              _curFilePath = strFilePath;

              if(_arrJSCompileFile.indexOf(_curFilePath) !== -1) {
                _totalComplete++;
              }

              if(_totalComplete === _arrJSCompileFile.length) {
                if(!GulpTaskStore.get(STATE_KEYS.is_js_finish)) {
                  // NOTE Đánh dấu lượt compile đầu tiên đã hoàn thành
                  if(GulpTaskStore.get(STATE_KEYS.js_dependents).isFirstCompile) {
                    GulpTaskStore.get(STATE_KEYS.js_dependents).isFirstCompile = false;
                  }

                  GulpTaskStore.commit(MUTATION_KEYS.set_is_js_finish, true);
                } else {
                  const strErrKey = (filename === 'index.js' ? foldername + '.js' : filename);

                  if(GulpTaskStore.get(STATE_KEYS.is_js_finish)) {
                    // NOTE - Sau lần build đầu tiên sẽ tiến hành checkUpdateError
                    GulpTaskStore.get(STATE_KEYS.handler_error_util).checkClearError(_isError, ARR_FILE_EXTENSION.JS, strErrKey);
                    GulpTaskStore.get(STATE_KEYS.handler_error_util).reportError();
                    GulpTaskStore.get(STATE_KEYS.handler_error_util).notifSuccess();

                    _isError = false;
                  }

                  const curFileName = APP.tmp.js + '/' + filename + '.js';
                  const clearCacheFileName = curFileName.replace('.js', Date.now() + '.js');

                  // NOTE - Xoá cache file bị trên window
                  try {
                    fs.renameSync(curFileName, clearCacheFileName);
                    fs.renameSync(clearCacheFileName, curFileName);
                  } catch(err) {}

                  // NOTE - Hiện tại đang sử dụng reload watch file change trực tiếp từ browsersync
                  // browserSync.reload(
                  //   { stream: true }
                  // );
                }

                _totalComplete = 0;
                _arrJSCompileFile = [];
              }
            });
          });
        } // _compileLoop()

        function _generateJsTargetAfterCssChange(path, filename, foldername) {
          let jsTargetPath = path.split(filename)[0];

          if(jsTargetPath.indexOf('partial') === -1) {
            jsTargetPath+='index.js';
          } else {
            jsTargetPath+='the-'+filename.replace('-style.css','.js');
          }

          return jsTargetPath;
        } // _generateJsTargetAfterCssChange()
      }
    }
  }; // getTmp()

  getDist() {
    const self = this;
    return {
      name: 'jsDist',
      init: function() {
        modules.gulp.task('jsDist', function() {
          if(
            1===2 &&
            !_isEmpty(GulpTaskStore.get(STATE_KEYS.tmp_construct)[ARR_FILE_EXTENSION.JS])
          ) {
            return GulpTaskStore.get(STATE_KEYS.move_file)(
              {
                'sourcePathUrl': APP.tmp.js + '/*.' + ARR_FILE_EXTENSION.JS,
                'targetPathUrl': APP.dist.js,
                'compressModule': uglify({
                  compress: {
                    sequences: true,
                    properties: true,
                    dead_code: true,
                    drop_debugger: true,
                    comparisons: true,
                    conditionals: true,
                    evaluate: true,
                    booleans: true,
                    loops: true,
                    unused: true,
                    hoist_funs: true,
                    if_return: true,
                    join_vars: true,
                    negate_iife: true,
                    drop_console: true
                  }
                }),
              }
            );
          } else {
            const _JS_COMPILE_FILE_LIST = [
              APP.src.js + '/**/*.js',
              APP.src.js + '/partial/**/*.vue',
            ];

            return modules.gulp.src(_JS_COMPILE_FILE_LIST)
            .pipe(modules.tap(function(file) {
              const filePath = file.path.replace(/\\/g, '/');
              // NOTE split file.path và lấy tên file cùng tên folder để rename đúng tên cho file js phía tmp
              let filename = filePath.split('/').slice(-2)[1];
              const foldername = filePath.split('/').slice(-2)[0];

              if(
                !(filename === 'app.js' ||
                (
                  filename.indexOf('-page.js') !== -1 &&
                  foldername.indexOf('-page') !== -1
                ))
              ) {
                return;
              }

              modules.browserify({ entries: [filePath] }) // path to your entry file here
              .transform(modules.vueifyBabel7)
              .transform(modules.babelify, {
                "presets": ["@babel/preset-env"],
              })
              .transform("aliasify")
              .external('vue') // remove vue from the bundle, if you omit this line whole vue will be bundled with your code
              .plugin(tinyify)
              .bundle()
              .pipe(modules.source(self._generateOtherAppName(filename, foldername)))
              .pipe(modules.print(
                filepath => {
                  return modules.ansiColors.yellow(`compile js: ${filepath}`);
                }
              ))
              .pipe(modules.buffer())
              // .pipe(uglify({
              //   compress: {
              //     sequences: true,
              //     properties: true,
              //     dead_code: true,
              //     drop_debugger: true,
              //     comparisons: true,
              //     conditionals: true,
              //     evaluate: true,
              //     booleans: true,
              //     loops: true,
              //     unused: true,
              //     hoist_funs: true,
              //     if_return: true,
              //     join_vars: true,
              //     negate_iife: true,
              //     drop_console: true
              //   }
              // }))
              .pipe(
                modules.gulp.dest(APP.dist.js)
              );
            }))
          }
        });
      }
    }
  }; // getDist()

  private _generateOtherAppName(filename, foldername) {
    if(
      filename.indexOf('app.js') !== -1 &&
      filename.indexOf('index.js') !== -1
    ) {
      return filename;
    }

    if(
      filename.indexOf('app.js') === -1 ||
      foldername === 'js'
    ) {
      return filename;
    }

    if(foldername.indexOf('page') !== -1) {
      return foldername + '.' + ARR_FILE_EXTENSION.JS;
    }

    return foldername + '-' + filename;
  } // _generateOtherAppName()
};
