import { isEmpty as _isEmpty } from 'lodash';

import modules, { browserSync, fs } from '@common/define/module-define';
import APP from '@common/enum/source-enum';
import {
  STATE_KEYS,
  GulpTaskStore,
} from '@common/gulp-task/store';
import { ARR_FILE_EXTENSION } from '@common/define/file-define';
import {
  RESOURCE,
  BASE_URL,
  BASE_STATIC_URL,
  BASE_API_URL
} from '@common/config/resource-config';

export default class DummyDataTask {
  constructor() {};

  getTmp() {
    return {
      name: 'dummyData',
      init:  function() {
        modules.gulp.task('dummyData', function() {
          let _isError = false;

          // NOTE - Define enviroment method for nunjucks render
          const _manageEnviroment = function(env) {
            env.addFilter('json', function (value, spaces) {
              if (value instanceof modules.nunjucksRender.nunjucks.runtime.SafeString) {
                value = value.toString();
              }
              const jsonString = JSON.stringify(value, null, spaces).replace(/</g, '\\u003c');
              return modules.nunjucksRender.nunjucks.runtime.markSafe(jsonString);
            });

            env.addFilter('isString', function (val) {
              return typeof val === 'string';
            });

            env.addFilter('isEmpty', function (val) {
              return _isEmpty(val);
            });
          };

          const _DUMMY_FILE_PATH = [
            APP.src.dummy_data + '/data-store/*.json'
          ];

          return modules.gulp.src(_DUMMY_FILE_PATH)
          .pipe(modules.cached('.json'))
          .pipe(modules.tap(function(file) {
            if(GulpTaskStore.get(STATE_KEYS.is_first_compile_all)) {
              return;
            }

            let filePath = file.path.replace(/\\/g, '/');

            // NOTE split file.path và lấy tên file cùng tên folder để rename đúng tên cho file njk phía tmp
            const filename = filePath.split('/').slice(-2)[1].replace('.json','');

            const njkFilename = RESOURCE?.dummy_data_name_map?.[filename] ?? filename;

            filePath = APP.src.njk + '/template/' + njkFilename + '.' + ARR_FILE_EXTENSION.NJK;

            modules.gulp.src(filePath)
            .pipe(modules.print(
              filepath => {
                return modules.ansiColors.yellow(`convert njk: ${filepath}`);
              }
            ))
            .pipe(modules.data(() => {
              let responseData:any = {};

              if(RESOURCE.resource[njkFilename]?.dummy_data) {
                responseData = GulpTaskStore.get(STATE_KEYS.dummy_data_manager).get(filename) || {};
              }

              if(
                !_isEmpty(responseData) &&
                !responseData.success
              ) {
                _isError = true;

                GulpTaskStore.get(STATE_KEYS.handler_error_util).handlerError(responseData, ARR_FILE_EXTENSION.JSON, GulpTaskStore.get(STATE_KEYS.is_first_compile_all));
              } else {
                GulpTaskStore.get(STATE_KEYS.handler_error_util).checkClearError(_isError, ARR_FILE_EXTENSION.JSON, filename + '.' + ARR_FILE_EXTENSION.JSON);
              }

              responseData = (_isError ? {} : responseData.data);

              return {
                file: njkFilename,
                namepage: njkFilename,
                data: responseData,
                CACHE_VERSION: GulpTaskStore.get(STATE_KEYS.update_version),
                BASE_URL,
                BASE_API_URL,
                API_VERSION: RESOURCE.api_version,
                EVN_APPLICATION: process.env.NODE_ENV,
                LAYOUT_CONFIG: {
                  'imageUrl' : BASE_STATIC_URL + '/image', // NOTE - Vì image sử dụng trong layout config cho những file render numjuck sang html thường có dạng '{{ LAYOUT_CONFIG.imageUrl }}/fantasy-image08.jpg' nên để dev tự thêm / sẽ clear hơn khi sử dụng với nunjuck
                  'cssUrl' : BASE_STATIC_URL + '/tmp/css',
                  'jsUrl' : BASE_STATIC_URL + '/tmp/js',
                  'languageUrl': BASE_STATIC_URL + '/language',
                }
              }
            }))
            .pipe(modules.nunjucksRender({
              ext: '.html',
              data: {
                objGlobal: RESOURCE,
                intRandomNumber : Math.random() * 10
              },
              manageEnv: _manageEnviroment,
            }))
            .on('error', function(err) {
              this.emit('end');
            })
            .pipe(modules.rename(function(path) {
              path.basename = njkFilename;
            }))
            .pipe(modules.gulp.dest(APP.tmp.path))
            .on('end', function() {
              if(!GulpTaskStore.get(STATE_KEYS.is_first_compile_all)) {
                // NOTE - Sau lần build đầu tiên sẽ tiến hành checkUpdateError
                const strErrKey = njkFilename + '.' + ARR_FILE_EXTENSION.NJK;

                GulpTaskStore.get(STATE_KEYS.handler_error_util).checkClearError(_isError, ARR_FILE_EXTENSION.NJK, strErrKey);
                GulpTaskStore.get(STATE_KEYS.handler_error_util).reportError();
                GulpTaskStore.get(STATE_KEYS.handler_error_util).notifSuccess();

                _isError = false;

                const curFileName = APP.tmp.path + '/' + njkFilename + '.html';
                const clearCacheFileName = curFileName.replace('.html', Date.now() + '.html');

                // NOTE - Xoá cache file bị trên window
                try {
                  fs.renameSync(curFileName, clearCacheFileName);
                  fs.renameSync(clearCacheFileName, curFileName);
                } catch(err) {}

                // NOTE - Hiện tại đang sử dụng reload watch file change trực tiếp từ browsersync
                // browserSync.reload(
                //   { stream: false }
                // );
              }
            })
          }));
        })
      }
    }
  }; // getTmp()
};
